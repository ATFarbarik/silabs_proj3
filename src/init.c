

#include <compiler_defs.h>
#include <C8051F912_defs.h>            // SFR declarations
#include <stdio.h>


SBIT (Emitter_ENABLE, SFR_P1, 5);          // Open-Drain output. Write '0' to 
                                       // enable the emitter.


void PORT_Init (void);
void SYSCLK_Init (void);
void Timer2_Init (void);


//-----------------------------------------------------------------------------
// Initialization Subroutines
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
// PORT_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Configure the Crossbar and GPIO ports.
//
// P0.0             D-input       Charger Detection (high = charger is attached)
// P0.4   digital   push-pull     UART TX
// P0.5   digital   open-drain    UART RX
//
//
// P1.0   analog    open-drain    Opto Sensor INput
// P1.1   analog    open-drain    Audio Input
// P1.4   analog    openD         Voltage / Battery Monitor
//
// P1.5             open-drain    Emitter enable
// P1.6                           Shutdown (active low) of the Voltage Regulators.
//
//-----------------------------------------------------------------------------

void PORT_Init (void)
{
 	Emitter_ENABLE = 0;

	// Optical Sensor input
	P1MDIN  &= ~0x01;                   // P1.0 analog input
	P1MDOUT &= ~0x01;                   // P1.0 open-drain
	P1      |=  0x01;					// set P1.0 latch
	P1SKIP  |=  0x01;                   // skip on the crossbar

	// Audio Sensor input
	P1MDIN  &= ~0x02;                   // P1.1 analog input
	P1MDOUT &= ~0x02;                   // P1.1 open-drain
	P1      |=  0x02;					// set P1.1 latch
	P1SKIP  |=  0x02;                   // skip on the crossbar

	// Voltage Monitor input
	P1MDIN  &= ~0x10;                   // P1.4 analog input
	P1MDOUT &= ~0x10;                   // P1.4 open-drain
	P1      |=  0x10;					// set P1.4 latch
	P1SKIP  |=  0x10;                   // skip on the crossbar



	P0MDOUT |= 0x10;                    // Enable UTX as push-pull output
 	XBR0    = 0x01;                     // Enable UART on P0.4(TX) and P0.5(RX)

    P1MDOUT |= 0x40;                    // P1.6 IS PUSH-PULL
	// Configure Crossbar
	XBR2    = 0x40;                     // Enable crossbar and weak pull-ups


}






//-----------------------------------------------------------------------------
// Timer2_Init
//-----------------------------------------------------------------------------

#define SYSCLK       		24500000    // SYSCLK frequency in Hz
#define TIMER_PRESCALER     12  		// Based on Timer2 CKCON and TMR2CN
 	                                    // settings
 
#define SAMPLERATE   		50         // ADC word rate
#define ADC_Sample_States    4

#define TIMER_TICKS_PER_MS  (SYSCLK/TIMER_PRESCALER/(SAMPLERATE * ADC_Sample_States))

//
// Return Value:  None
// Parameters:    None
//
// Configure Timer2 to 16-bit auto-reload and generate an interrupt at 100uS
// intervals.  Timer 2 overflow automatically triggers ADC0 conversion.
//
//-----------------------------------------------------------------------------
void Timer2_Init (void)
{
  CKCON &= ~0x60;                     // Timer2 uses SYSCLK/12
   TMR2CN &= ~0x01;


   TMR2RL  = 65535 - (TIMER_TICKS_PER_MS); // init reload value 
   TMR2    = TMR2RL;                   // init timer value
   
   TMR2CN = 0x04;                      // Enable Timer2 in auto-reload mode
   ET2 = 1;                            // Timer2 interrupt enabled

}




//-----------------------------------------------------------------------------
// SYSCLK_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// This routine initializes the system clock to use the internal 24.5MHz / 8
// oscillator as its clock source.   and enables the missing clock 
// detector.
// 
//-----------------------------------------------------------------------------

void SYSCLK_Init (void)
{
   
   OSCICN |= 0x80;                     // Enable the precision internal osc.
   
   RSTSRC = 0x06;                      // Enable missing clock detector and
                                       // leave VDD Monitor enabled.

   CLKSEL = 0x00;                      // Select precision internal osc. 
                                       // divided by 1 as the system clock
   
}
