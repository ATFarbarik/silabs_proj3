//-----------------------------------------------------------------------------
// AT_PROJ1_MAIN.C
//-----------------------------------------------------------------------------
// Copyright 2009 Silicon Laboratories, Inc.
// http://www.silabs.com
//
// Copyright 2013 M.Farbarik and Articulate Technologies
//
// Program Description:
//
// This program lights up the yellow LED on the C8051F91x target board when
// the capacitive touch sense (contactless) switch (P1.0) is touched. It 
// also lights up the red LED when the (contactless) switch (P1.1) is touched. 
//
// A relaxation oscillator is implemented using an on-chip analog comparator 
// and external resistors R15 through R19. The high-to-low transitions of the 
// relaxation oscillator are counted by Timer0. The relaxation oscillator 
// frequency depends on the capacitance of the touch sense trace capacitor. 
// The count of Timer0 is periodically checked on every Timer2 overflow. 
// Depending on the count, it can be determined whether SW3 is being touched
// or not. 
//
//
// How To Test:
//
// Setup:
// 1) Download code to the 'F912 target board
// 2) Ensure that pins 1-2, 3-4, 5-6, and 7-8 are shorted on the J8 header
//
// One Time Calibration (stored in non-volatile flash):
// 1) Both LEDs will be lit. Place a finger on either Touch Sense Pad (P1.0
//    or P1.1), and  hold it there. Do not remove this finger till step 3.

// 2) Press and hold switch SW2 (P0.2) for if your finger is on Touch Sense 
//    Pad (P1.0). Otherwise press and hold SW3 (P0.3) if your finger is on 
//    Touch Sense Pad (P1.1).  Release the switch after 1 second.
// 3) The yellow LED will continue to be lit if your finger was touching the 
//    P1.0 pad. The red LED will continue to be lit if your finger was touching
//    the P1.1 pad. Remove finger from the Touch Sense Pad. The LEDs should 
//    switch off.
//
// Usage:
// 1) Touch SW10. The Yellow LED (P1.6) should light up in response.
// 2) Touch SW11. The Red LED (P1.5) should light up in response.
//
// Target:         C8051F912
// Tool chain:     Generic
// Command Line:   None
//
// Release 1.2 
//    - Compiled and tested for Raisonance Toolchain (FB)
//    - 17 MAY 10
//
// Release 1.1
//    - Compile and test C8051F912_defs.h on 912 board (JH)
//    - Port to C8051F912-TB from C8051F930-TB (JH)
//    - 06 JULY 2009
//
// Release 1.0
//    - Initial Revision (FB)
//    - 05 OCT 2007
//
/*
	Revision	Description
	19 Nov 2013	Move away from Silabs 912 board to AT board1 (custom)
				Add code for audio detection.

	20 Jan 2014 disable blinking LEDs
*/


//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

#include <compiler_defs.h>
#include <C8051F912_defs.h>            // SFR declarations
#include <stdio.h>

//-----------------------------------------------------------------------------
// Pin Declarations
//-----------------------------------------------------------------------------

//SBIT (RED_LED,    SFR_P1, 5);          // '0' means ON, '1' means OFF
SBIT (RED_LED,    SFR_P0, 1);          // '0' means ON, '1' means OFF

//SBIT (YELLOW_LED, SFR_P1, 6);          // '0' means ON, '1' means OFF
SBIT (YELLOW_LED, SFR_P0, 2);          // '0' means ON, '1' means OFF

//SBIT (SW2,        SFR_P0, 2);          // SW2 == 0 means switch pressed


//SBIT (SW3,        SFR_P0, 3);          // SW3 == 0 means switch pressed

//-----------------------------------------------------------------------------
// Global CONSTANTS
//-----------------------------------------------------------------------------

#define SYSCLK      24500000           // SYSCLK frequency in Hz
#define BAUDRATE      115200           // Baud rate of UART in bps

#define LED_ON              0          // Macros to turn LED on and off
#define LED_OFF             1

#define SW11_SENSITIVITY   50         // Sensitivity value used for both
                                       // Touch Sense switches. Larger values
                                       // make the switches more sensitive

#define CAL_ADDRESS         0          // Address in the scratchpad to store
                                       // the calibrated switch value

#define SCRATCHPAD          1          // Passed to the Flash access routines
                                       // to indicate that the calibration
                                       // constant is located in the scratchpad
               

//-----------------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------------
// Timer2 count of SW20 relaxation oscillator transitions
UU16 SW10_Timer_Count;                     
UU16 SW11_Timer_Count;

// Touch Switch: SW20==0 means finger sensed; SW20==1 means finger not sensed.	
U8 SW10_Status;     
U8 SW11_Status; 

//-----------------------------------------------------------------------------
// Function PROTOTYPES
//-----------------------------------------------------------------------------

extern void SYSCLK_Init (void);
extern void PORT_Init (void);
extern void TouchSense_Init (void);

extern void Wait_MS(unsigned int ms);
extern int  TouchSense_Update(void);
extern int  TouchSense_UpdateX (unsigned int i);
extern void Calibrate (void);
extern U16  Get_Calibration(void);
extern void TouchSense_Update_ExtendedMath (void);

extern void UART0_Init (void);

extern void ADC0_Initialize (void);
extern int  ADC0_ReadConversion (void);
extern void ADC0_StartConversion (void);


//-----------------------------------------------------------------------------
// MAIN Routine
//-----------------------------------------------------------------------------

void main (void)
{
	char mode;     			// mode=0  CapTouch,    mode=1 IR sensor
	int audioValue;
	int touchValue;

    PCA0MD &= ~0x40;           // WDTE = 0 (clear watchdog timer enable)
    PORT_Init  ();             // Initialize Port I/O
    SYSCLK_Init();             // Initialize Oscillator
    UART0_Init ();
    ADC0_Initialize  ();

	   
    TouchSense_Init();         // Initialize Comparator0 and 
                               // Timer2 for use with TouchSense
   
    Calibrate ();
    printf("calibration:  %d \n", Get_Calibration() );
    printf("hello \n");

    while(1)
    {
 		Wait_MS(9);                     // Polling Interval 
	    
				       
		ADC0_StartConversion();
		touchValue = TouchSense_UpdateX(10);             // Update switch variables
		audioValue = ADC0_ReadConversion();


		printf("0,%d,%d\n", touchValue,audioValue);
 
        YELLOW_LED = SW10_Status;        // Set LED states based on the 
        
   }

}


