//-----------------------------------------------------------------------------
// AT_PROJ2_MAIN.C       optical sensing
//-----------------------------------------------------------------------------
// Copyright 2009 Silicon Laboratories, Inc.
// http://www.silabs.com
//
// Copyright 2014 M.Farbarik and Articulate Technologies
//

/*
	Revision	Description
	20 MAY 2014 COPY FROM PROJ2.  NEW PCBA, #3,
                Enable LEDS
                Enable Shutdown control line for linear regulators.

    21 May 2014 Vmonitor (battery) at P1.4
`
    08 Jun 2014 Add switches in build for OldPlatform vs. NewPlatform
                The old being schematic1, USB powered.
*/


//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

#include <compiler_defs.h>
#include <C8051F912_defs.h>            // SFR declarations
#include <stdio.h>
#include <math.h>

//-----------------------------------------------------------------------------
// Pin Declarations
//-----------------------------------------------------------------------------
SBIT (RED_LED,   SFR_P0, 1);          // '0' means ON, '1' means OFF
SBIT (GREEN_LED, SFR_P0, 2);          // '0' means ON, '1' means OFF

// Power Enable / Control line to the other Linear Regulators
SBIT (SHDN, SFR_P1, 6);                 // 1 = ON

//-----------------------------------------------------------------------------
// Global CONSTANTS
//-----------------------------------------------------------------------------

#define SYSCLK      24500000           // SYSCLK frequency in Hz
#define BAUDRATE      115200           // Baud rate of UART in bps

#define LED_ON              0          // Macros to turn LED on and off
#define LED_OFF             1

#define TRUE  	1
#define FALSE 	0

#define LOWBATTERY_THRESHOLD_3p5V       540
#define BATTERYFULLYCHARGED_4p15V       644
#define BATTERYHASENOUGHCHARGE2RUN      600
/* vBattery     ADC
        3	    465
        3.1	    481
        3.2	    496
        3.3	    512
        3.4	    528
        3.5	    543
        3.6	    559
        3.7	    574
        3.8	    590
        3.9	    605
        4	    621
        4.1	    636
        4.2	    652
*/
/*
    oldPlatform does not have:
        shutdown and voltage monitoring.
        voltage regulation  >> thus, the leds need to be disabled.

    NewPlatform has
        voltage monitoring
        voltage regulation
        battery
        
*/
#define NewPlatform
//#define OldPlatform

#define State_Startup       0 
#define State_Charging      1 
#define State_FullyCharged  2
#define State_WaitForCom    3
#define State_Run           4
#define State_LowBattery    5               

//-----------------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------------
unsigned char T2ISR;


//-----------------------------------------------------------------------------
// Function PROTOTYPES
//-----------------------------------------------------------------------------
extern void SYSCLK_Init (void);
extern void PORT_Init (void);
extern void Timer2_Init (void);

extern void Wait_MS(unsigned int ms);
extern int  OpticalSense_Update(void);

extern void UART0_Init (void);

extern void ADC0_Initialize (void);
extern int  ADC0_AudioChannelConversion (void);
extern void ADC0_StartConversion (void);
extern int  ADC0_SensorChannelConversion (void);
extern void ADC0_MuxSetSensor (void);
extern void ADC0_MuxSetAudio  (void);
extern int  ADC0_VoltageMonitorConversion (void);
extern void ADC0_MuxSetVmonitor (void);



void FlashingLEDs (unsigned char);
char Running (void);

INTERRUPT_PROTO (ADC_ISR, INTERRUPT_ADC0_EOC);
INTERRUPT_PROTO (TIMER2_ISR, INTERRUPT_TIMER2);


SBIT (Emitter_ENABLE, SFR_P1, 5);    
SBIT (ChargerDetect , SFR_P0, 0);


//-----------------------------------------------------------------------------
// MAIN Routine
//-----------------------------------------------------------------------------
void main (void)
{
	
    double logVal;
    unsigned char i = 0;
    unsigned char state = 0;

    PCA0MD &= ~0x40;           // WDTE = 0 (clear watchdog timer enable)
    PORT_Init  ();             // Initialize Port I/O
    SYSCLK_Init();             // Initialize Oscillator
	Timer2_Init();
    UART0_Init ();
    ADC0_Initialize  ();
    ADC0_MuxSetVmonitor (); // prepare for Battery Voltage measurement
    SHDN = 0;

#ifdef NewPlatform    
    state = State_Startup;
#endif

#ifdef OldPlatform
    state = State_Run;
#endif
 
    printf("hello \n");
	EA = 1;

    while(1)
    {
 		if (T2ISR == TRUE)
		{
	    	
#ifdef NewPlatform
            FlashingLEDs (state);
            if (ChargerDetect)  state = State_Charging;
#endif
            
	        switch (state)
            {
                case State_Startup:
                    if (ADC0_VoltageMonitorConversion () > BATTERYHASENOUGHCHARGE2RUN)
                    {   
                        state = State_Run;
                        ADC0_MuxSetSensor (); // prepare for the Sensor sample
                    }
                    else
                        state = State_LowBattery;
                    break;

                case State_Charging:
                    //printf("-C-\n\r");
                    SHDN = 0;


                    if (ChargerDetect == 0) state = State_Startup;


                    else if (ADC0_VoltageMonitorConversion () > BATTERYFULLYCHARGED_4p15V)
                        state = State_FullyCharged;
                    break;

                case State_FullyCharged:
                    SHDN = 0;
                    if (ChargerDetect == 0) state = State_Startup;
                    break;
                
                case State_WaitForCom:
                
                case State_Run:
                    SHDN = 1;
                    if (Running () < 0)
                    {   // low battery condition is returned as -1
                        state = State_LowBattery;

                    }    ;
                    break;

                
                case State_LowBattery:
                    // turn off stuff
                    SHDN = 0;
                    break;
                

                default:
                    break;
            }
            
            T2ISR = FALSE;           
			
		}
        
   }

}


/* ============================================================== */
char Running (void)
{
    static unsigned char x = 0x01;
    static int audioValue, opticalValue_dark, opticalValue_bright, vBattery;
    static char arr[8]={0};
    char returnValue = 0;
    
                 
    if (x == 0x01)			       
	{
	    opticalValue_dark = 1023 - ADC0_SensorChannelConversion();
        //ticalValue_dark = 0;
        Emitter_ENABLE = 0;  // Emitter ON  
        x = 0x02; 
        
       
    }
    else if (x == 0x02)
    {
        opticalValue_bright = 1023 - ADC0_SensorChannelConversion(); 
        //opticalValue_bright = 123;
        Emitter_ENABLE = 1;  // Emitter OFF
            
        ADC0_MuxSetAudio (); // prepare for the Audio sample
        x = 0x03;

      
    }     
    else if (x == 0x03)
    {
        audioValue  = ADC0_AudioChannelConversion();
    			                                
//        printf("%4d\r\n", audioValue);
        sprintf(arr,"%3d%3d",(opticalValue_bright - opticalValue_dark), audioValue);
        //sprintf(arr,"%3d%3d",i, audioValue);
 

        ADC0_MuxSetVmonitor();
        x = 0x04;
    } 
    else
    {

#ifdef NewPlatform
        vBattery = ADC0_VoltageMonitorConversion ();   
        //printf(" %d \r\n", vBattery);             
        if (vBattery <LOWBATTERY_THRESHOLD_3p5V)
            returnValue = -1; 
#endif
                
        ADC0_MuxSetSensor (); // prepare for the Sensor sample
        x = 0x01;

        putchar(arr[0]); 
        putchar(arr[1]); 
        putchar(arr[2]); 
        putchar(arr[3]); 
        putchar(arr[4]); 
        putchar(arr[5]); 
        putchar(0x0D);
    }

    return(returnValue);
}



/* ============================================================== */
void FlashingLEDs (unsigned char state)
{
    static unsigned char i = 0;
    i++;

    switch (state)
    {
        case State_Startup:
            break;

        case State_Charging:
            GREEN_LED = LED_OFF;
            switch (i)
            {
                case 1:
                case 81:
                case 134:
                case 170:
                case 193:
                case 209:
                case 219:

                
                    RED_LED = LED_ON;
                    break;
                case 250:
                    i = 0;
                    break;
                default:
                    RED_LED = LED_OFF;
            }
            break;

                        
        case State_FullyCharged:
            GREEN_LED = LED_OFF;
            RED_LED = LED_ON;
            break;


        case State_WaitForCom:
        case State_Run:
        {
            RED_LED = LED_OFF;
            switch (i)
            {
                case 1:
                case 2:
                case 50:
                case 51:
                    GREEN_LED = LED_ON;
                    break;
                case 150:
                    i = 0;
                    break;
                default:
                    GREEN_LED = LED_OFF;
            }
            break;

        }

        case State_LowBattery:
        {
            GREEN_LED = LED_OFF;
            switch (i)
            {
                case 1:
                case 2:
                case 3:
                    RED_LED = LED_ON;
                    break;
                case 200:
                    i = 0;
                    break;
                default:
                    RED_LED = LED_OFF;
            }
            break;
        }

        default:
            ;
    }
   
}


//-----------------------------------------------------------------------------
// Interrupt Service Routines
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
// ADC0_ISR
//-----------------------------------------------------------------------------
//
//
//-----------------------------------------------------------------------------
/*
		INTERRUPT (ADC_ISR, INTERRUPT_ADC0_EOC)
		{

		   unsigned long result;
		   unsigned long mV;                   // measured voltage in mV
		   static unsigned char sample_count = 0;   

		   AD0INT = 0;                         // clear ADC0 conv. complete flag
   
		   result = ADC0;                      // copy ADC0 into result
 
		   // The 12-bit ADC value represents the voltage applied
		   // to P0.6 according to the following equation:
		   //
		   //                           Vref (mV)
		   //   measurement (mV) =   --------------- * result (bits)
		   //                       ((2^10)-1)*2^2 (bits)

		   mV =  result * 3300 / 4092;
   
		   // Print the results every 10 samples
		   if(sample_count >= 10)
		   {
		      printf("\f\nP0.6 voltage: %d mV", (unsigned int) mV);
		      sample_count = 0;
   
		   } else
		   {
		      sample_count++;
		   }
		}
*/

//-----------------------------------------------------------------------------
// Timer2_ISR
//-----------------------------------------------------------------------------
INTERRUPT (TIMER2_ISR, INTERRUPT_TIMER2)
{
   T2ISR = TRUE;
   TF2H = 0;                           // Reset Interrupt
}

