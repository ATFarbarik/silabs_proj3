//-----------------------------------------------------------------------------
// F912_ADC0_12bit_ExternalInput.c
//-----------------------------------------------------------------------------
// Copyright 2007 Silicon Laboratories, Inc.
// http://www.silabs.com
//
// Program Description:
// --------------------
//
// This example code uses ADC in 12-bit mode to take analog measurements from 
// input  P0.6 (Potentiometer input), then prints the results to a terminal 
// window via the UART.
//
// The system is clocked by the internal 24.5MHz oscillator.  Timer 2 triggers
// a conversion on ADC0 on each overflow.  The completion of this conversion
// in turn triggers an interrupt service routine (ISR).  
//
// The analog multiplexer selects P0.6 as the ADC0 input. All samples are 
// single-ended with respect to GND. P0.6 is configured as an analog input 
// in the port initialization routine.
//
// When J16 is shorted, the potentiometer (R14) wiper is connected to P0.6.
// When J16 is open, P0.6 may also be supplied with a voltage from the H1 
// Terminal block. 
//
// Terminal output is done via printf, which directs the characters to
// UART0.  A UART initialization routine is therefore necessary.
//
// ADC Settling Time Requirements, Sampling Rate:
// ----------------------------------------------
//
// The total sample time per input is comprised of an input setting time
// (Tsettle), followed by a conversion time (Tconvert):
//
// Tsample  = Tsettle + Tconvert
//
// |--------Settling-------|==Conversion==|----Settling--- . . .
// Timer 2 overflow        ^
// ADC0 ISR                               ^
//
// The ADC input voltage must be allowed adequate time to settle before the
// conversion is made.  This settling depends on the external source
// impedance, internal mux impedance, and internal capacitance.
// Settling time is given by:
//
//                   | 2^n |
//    Tsettle =   ln | --- | * Rtotal * Csample
//                   | SA  |
//
// In this application, assume a 100kohm potentiometer as the voltage divider.
// The expression evaluates to:
//
//                   | 2^10 |
//    Tsettle =   ln | ---- | * 105e3 * 5e-12 = 4.4uS
//                   | 0.25 |
//
// In addition, one must allow at least 1.7uS after changing analog mux
// inputs or PGA settings.  The settling time in this example, then, is
// dictated by the large external source resistance.
//
// The conversion is 10 periods of the SAR clock <SAR_CLK>.  At 8.33 MHz,
// this time is 10 * 120nS = 1.2 uS.
//
//
// Tsample, minimum  = Tsettle + Tconvert
//                   = 4.4uS + 1.2uS
//                   = 5.6 uS
//
// Timer 2 is set to start a conversion every 100uS
//
// Resources:
// ---------------
// Timer1: clocks UART
// Timer2: overflow initiates ADC conversion
//
// How To Test:
// ------------
// 1) Download code to a device on a development board
// 2) Ensure that jumpers are placed on J12 of the  target board
//    that connect P0.4/TX to the RXD signal, P0.5/RX to the TXD signal,
//    and VDD/DC+ to VIO.
// 3) Connect a USB cable from the development board to a PC
// 4) On the PC, open HyperTerminal (or any other terminal program) and connect
//    to the USB port (virtual com port) at <BAUDRATE> and 8-N-1
// 5) Connect a variable voltage source to P0.6 or verify that J15 and J16 are
//    installed. J15 should be connected to the side labeled P1.4.
// 6) HyperTerminal will print the voltage measured by the device if
//    everything is working properly
//
// Target:         C8051F912 and C8051F902 only
// Tool chain:     Generic
// Command Line:   None
//
// Release 1.0
//    - Initial Revision (FB)
//    - 17 MAY 10

//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

#include <compiler_defs.h>
#include <C8051F912_defs.h>            // SFR declarations
#include <stdio.h>

//-----------------------------------------------------------------------------
// Global CONSTANTS
//-----------------------------------------------------------------------------

#define SYSCLK       24500000          // SYSCLK frequency in Hz
#define BAUDRATE     115200            // Baud rate of UART in bps



//-----------------------------------------------------------------------------
// Function PROTOTYPES
//-----------------------------------------------------------------------------
void ADC0_Initialize (void);
int  ADC0_ReadConversion (void);
void ADC0_StartConversion (void);



//-----------------------------------------------------------------------------
// ADC0_Init
//-----------------------------------------------------------------------------
//
// Return Value:  None
// Parameters:    None
//
// Configures ADC0 to make single-ended analog measurements on pin P0.6
//
//-----------------------------------------------------------------------------
void ADC0_Initialize (void)
{  
   //ADC0CN = 0x42;                      // ADC0 disabled, Burst Mode enabled,
                                       // conversion triggered on TMR2 overflow

	AD0EN = 0;		// DISABLE ADC0
	BURSTEN = 1;	// Burst mode enabled
	AD0INT = 0;                         // clear ADC0 conv. complete flag
	AD0CM0 = 0;
	AD0CM1 = 0;
	AD0CM2 = 0;
  
   REF0CN = 0x18;                      // Select internal high speed voltage
                                       // reference
   //ADC0MX = 0x06;
   ADC0MX = 0x09;                      // Select P1.1 as the ADC input pin

   ADC0CF = ((SYSCLK/8300000))<<3;     // Set SAR clock to 8.3MHz

   //ADC0CF |= 0x00;                     // Select Gain of 0.5
   ADC0CF |= 0x01;						// gain 1
   
   ADC0AC = 0x81;                      // Right-justify results, shifted right
                                       // by 0 bits. Accumulate 4 samples for
                                       // an output word of 12-bits. 12-bit mode
                                       // is enabled

   //EIE1 |= 0x08;                       // Enable ADC0 conversion complete int.

   AD0EN = 1; 		// enable ADC0

}


//-----------------------------------------------------------------------------
void ADC0_StartConversion (void)
{
	AD0BUSY = 1;
}


//-----------------------------------------------------------------------------
int ADC0_ReadConversion (void)
{

   unsigned int result;
   //unsigned long mV;                   // measured voltage in mV
   //static unsigned char sample_count = 0;   

   AD0INT = 0;                         // clear ADC0 conv. complete flag
   AD0BUSY = 0;
   
   result = ADC0;                      // copy ADC0 into result
 
   // The 12-bit ADC value represents the voltage applied
   // to P0.6 according to the following equation:
   //
   //                           Vref (mV)
   //   measurement (mV) =   --------------- * result (bits)
   //                       ((2^10)-1)*2^2 (bits)

   // mV =  result * 3300 / 4092;
   
   return(result);
   
}

//-----------------------------------------------------------------------------
// End Of File
//-----------------------------------------------------------------------------
