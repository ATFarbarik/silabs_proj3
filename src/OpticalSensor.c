
#include <compiler_defs.h>
#include <C8051F912_defs.h>            // SFR declarations
#include <stdio.h>


int OpticalSense_Update (void);

void Wait_MS(unsigned int ms);


SBIT (RED_LED,    SFR_P1, 5);          // '0' means ON, '1' means OFF
SBIT (YELLOW_LED, SFR_P1, 6);          // '0' means ON, '1' means OFF
SBIT (SW2,        SFR_P0, 2);          // SW2 == 0 means switch pressed
SBIT (SW3,        SFR_P0, 3);          // SW3 == 0 means switch pressed


#define SYSCLK      24500000           // SYSCLK frequency in Hz



//-----------------------------------------------------------------------------
// TouchSense_Update
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Configure the Crossbar and GPIO ports.
//
//
//-----------------------------------------------------------------------------
int OpticalSense_Update (void)
{  
   
   UU16 timer_count_A, timer_count_B;
   int returnValue;
 

   // Prepare Timer2 for the first TouchSense reading
   TMR2CN &= ~0x80;                    // Clear overflow flag 
   while(!(TMR2CN & 0x80));            // Wait for overflow (this also doubles as the capture event flag)
   timer_count_A.U16 = TMR2RL;         // Record value
   
   // Prepare Timer2 for the second TouchSense reading
   TMR2CN &= ~0x80;                    // Clear overflow flag
   while(!(TMR2CN & 0x80));            // Wait for overflow
   timer_count_B.U16 = TMR2RL;         // Record value
   
   // Calculate the oscillation period
   SW10_Timer_Count.U16 = timer_count_B.U16 - timer_count_A.U16;

   // To measure the RC oscillator frequency, use this equation:
   //   RC Freq = 24.5MHz / (SW10_Timer_Count.U16 / 8)


	/*
	   // Update the status variable for SW20
	   if((SW10_Timer_Count.U16 > (Get_Calibration() + SW11_SENSITIVITY)) || 
	   		( mathResult > 10) )
	   {
	      	SW10_Status = 0;
	   	  	printf("1,");
	   } 
	   	else
	   {
	      SW10_Status = 1;
		  printf("0,");
	   }
	*/
   //printf("%4d\n", SW10_Timer_Count.U16 - Get_Calibration());

	returnValue = SW10_Timer_Count.U16;
	return(returnValue);	
}










//-----------------------------------------------------------------------------
// Wait_MS
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters:
//   1) unsigned int ms - number of milliseconds of delay
//                        range is full range of integer: 0 to 65335
//
// This routine inserts a delay of <ms> milliseconds.
//-----------------------------------------------------------------------------
void Wait_MS(unsigned int ms)
{
   char i;

   TR0 = 0;                            // Stop Timer0
   
   TMOD &= ~0x0F;                      // Timer0 in 8-bit mode
   TMOD |= 0x02;
   
   CKCON &= ~0x03;                     // Timer0 uses a 1:48 prescaler
   CKCON |= 0x02;                   
    
   
   TH0 = -SYSCLK/48/10000;             // Set Timer0 Reload Value to 
                                       // overflow at a rate of 10kHz
   
   TL0 = TH0;                          // Init Timer0 low byte to the
                                       // reload value
   
   TF0 = 0;                            // Clear Timer0 Interrupt Flag
   ET0 = 0;                            // Timer0 interrupt disabled
   TR0 = 1;                            // Timer0 on

   while(ms--)
   {
      for(i = 0; i < 10; i++)
      {
         TF0 = 0;
         while(!TF0);
      }
   }

   TF0 = 0;

}







